package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
)

func main() {
	dataPath := flag.String("dataPath", "", "Where to find the data folders and files")
	flag.Parse()
	if len(*dataPath) < 1 {
		cwd, err := os.Getwd()
		if err != nil {
			log.Fatalf("unable to determine current working directory: %v", err)
		}
		dataPath = &cwd
	}
	input, err := getInput(*dataPath, 2020, 1)
	if err != nil {
		log.Fatalf("could  not get input: %v", err)
	}
	vals := make([]int, 0)
	scanner := bufio.NewScanner(bytes.NewReader(input))
	for scanner.Scan() {
		val, err := strconv.Atoi(scanner.Text())
		if err != nil {
			log.Printf("invalid integer input value: %v\n", err)
			break
		}
		vals = append(vals, val)
	}
	answer := 0
	for i := 0; i < len(vals); i++ {
		for x := i + 1; x < len(vals); x++ {
			if (vals[i] + vals[x]) == 2020 {
				answer = vals[i] * vals[x]
				break
			}
		}
		if answer > 0 {
			break
		}
	}
	fmt.Printf("answer: %d\n", answer)
	answer = 0
	for i := 0; i < len(vals); i++ {
		for x := i + 1; x < len(vals); x++ {
			for y := x + 1; y < len(vals); y++ {
				if (vals[i] + vals[x] + vals[y]) == 2020 {
					answer = vals[i] * vals[x] * vals[y]
					break
				}
			}
			if answer > 0 {
				break
			}
		}
		if answer > 0 {
			break
		}
	}
	fmt.Printf("answer 2: %d\n", answer)
}

func getInput(dataPath string, year int, day int) ([]byte, error) {
	inputPath := filepath.Join(dataPath, "data", strconv.Itoa(year), fmt.Sprintf("%d.dat", day))
	input, err := ioutil.ReadFile(inputPath)
	if err != nil {
		return nil, fmt.Errorf("unable to open input file: %v", err)
	}
	return input, nil
}
